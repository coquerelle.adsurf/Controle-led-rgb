Controle de led RGB com Node.JS + socket.io + Johnny-five + Arduino
========================================================
*fork de l'application [pineli/Controle-led-rgb](https://github.com/pineli/Controle-led-rgb) (2015), transférée récemment*



## L'installation

```
git clone git@github.com:Pineli/Controle-led-rgb.git
cd Controle-led-rgb
npm install
```

Modules utilisés

* johnny-five
* express
* socket.io

La documentation de Johnny-Five peut être trouvée [ici](https://github.com/rwaldron/johnny-five/wiki)


Connectez maintenant votre Arduino au câble USB, téléchargez **Example > Firmdata > StandandFirmData**

## Execution de l'application

```
node appgnd.js
```
Dans le navigateur, ouvrez l'url:

```
localhost:3000
```
Selecione a cor preferida e a mesma será enviada via Websocket ao NodeJS/Johnny-five e que fará com que o led RGB reproduza a mesma cor, ou cor aproximada, a selecionada.


# Circuit utilisé

![Arduino](https://github.com/Pineli/johnny-five/raw/master/docs/breadboard/led-rgb.png)
[image tirée de la documentation officielle](https://github.com/rwaldron/johnny-five/blob/master/docs/led-rgb.md) 

# Démo

[![ScreenShot](https://i1.ytimg.com/vi/9Fcb31sa3uo/default.jpg)](https://www.youtube.com/watch?v=9Fcb31sa3uo&feature=youtu.be)

## License
Sous licence MIT.

-------------

## Modification

**ATTENTION** : il y a 2 type de LED RGB

l'application qui fonctionne pour les LED RGB masse commune => `node appgnd.js`

*Voir le schéma plus haut*

l'application qui fonctionne pour les LED RGB anode commune => `node app5v.js`

*Voir le schéma qui suit*

![Anduino](https://gitlab.com/coquerelle.adsurf/Controle-led-rgb/-/raw/master/doc/led-rgb_5v.png)
